const fb = require("facebook-chat-api");
const MongoClient = require("mongodb");

module.exports = class {
  constructor(config) {
    this.config = config;
  }

  init() {
    this.log(`Starting fb-rollbot ${this.config.version}`);
    MongoClient.connect(`mongodb://${this.config.db.host}/${this.config.db.database}`, (err, database) => {
      this.db = database;
      this.rolls = [];
      this.lastBank = 0;
      this.listen();
    });
  }

  listen() {
    fb({ email: this.config.email, password: this.config.password }, (err, api) => {
      if(err) return console.error(err);
      this.api = api;

      this.api.listen((err, message) => {
        //this.log(JSON.stringify(message))
        if(message.type == "message" && this.config.thread == message.threadID && typeof message.body == "string") {
          this.log("message event from selected threads")
          // handle chat message
          this.log("Handling chat message")
          this.handleMessage(message.senderID, message.body, (user) => {
            if(message.body.toLowerCase() == "/bank" && (Date.now() - this.lastBank) > 30*1000) {
              // bank
              this.log("Handling bank")
              this.handleBank((msg) => {
                this.api.sendMessage(msg, message.threadID);
              })
            } else if(/^\/roll ([\d]+|all)$/.test(message.body.toLowerCase())) {
              // roll
              this.log("Handling roll")
              this.handleRoll(user, message.body.toLowerCase().match(/^\/roll ([\d]+|all)$/)[1], (msg) => {
                this.api.sendMessage("🎰 " + msg, message.threadID);
              })
            } else this.messageMoney(message.senderID)
          });
        }
      });
    });
  }

  handleMessage(sid, msg, cb) {
    this.db.collection("fb-messages").save({ sid: sid, tid: this.config.thread, msg: msg, date: new Date() }); // store msg
    this.getUser(sid, (user) => {
      cb(user);
    })
  }

  messageMoney(sid) {
    this.moneyManager(sid, this.config.messageMoney, () => {
      this.log(`sid #${sid} received ${this.config.messageMoney}`)
    })
  }

  moneyManager(sid, amount, cb) {
    this.db.collection("fb-users").update({ sid: sid, tid: this.config.thread }, { $inc: { money: amount } }, (err) => {
      cb(err)
    });
  }

  handleBank(cb) {
    this.db.collection("fb-users").find({ tid: this.config.thread }).sort({ money: -1 }).limit(10).toArray((err, results) => {
      this.lastBank = Date.now()
      cb(this.formatBank(results))
    });
  }

  handleRoll(user, amount, out) {
    if(amount == "all") amount = user.money;
    else amount = parseInt(amount);
    this.log(`Got roll command from ${user.sid} with amount ${amount}`);
    if(user.money >= amount) {
      var base = this.inRoll(user.sid);
      var rollAmount = base + amount;
      this.moneyManager(user.sid, -amount, (err) => {
        if(base) {
          this.updateRoll(user.sid, rollAmount);
          out(`${user.firstName} vient d'augmenter son pari à 💲${rollAmount} (${this.winChance(rollAmount)}%)`)
        } else {
          if(amount >= this.config.minAmount) {
            this.rolls.push({ sid: user.sid, firstName: user.firstName, amount: rollAmount });
            var rollText = `${user.firstName} vient de parier 💲${rollAmount} (${this.winChance(rollAmount)}%) ! Tapez /roll pour parier à votre tour !`

            if(this.rolls.length == 2) {
              rollText += `\n⏲️ Roll dans 60 secondes !`;
              clearTimeout(this.rollEnd);
              this.countDown(out);
              setTimeout(() => {
                this.roll(out)
              }, 60000);
            } else if(this.rolls.length == 1) {
              this.rollEnd = setTimeout(() => {
                this.moneyManager(this.rolls[0].sid, this.rolls[0].amount, () => {
                  this.rolls = []
                  out(`Le roll a expiré 🙁`)
                })
              }, 300000)
            }
            out(rollText);
          } else {
            out(`Veuillez miser au moins 💲${this.config.minAmount}`)
          }
        }
      });
    } else {
      out("Vous n'avez pas assez de 💲 pour roll ce montant :)")
    }
  }

  updateRoll(sid, amount) {
    function findFromSid(roll) {
      return roll.sid === sid;
    }

    this.rolls[this.rolls.findIndex(findFromSid)].amount = amount;
  }

  inRoll(sid) {
    var final = 0;
    this.rolls.forEach((roll) => {
      if(roll.sid == sid) final = roll.amount;
    });

    return final;
  }

  roll(out) {
    this.log("Rolling !");
    var total = this.getTotal();
    this.log("Calculated total: " + total);

    var rand = Math.floor(Math.random() * total) + 1;
    this.log("Choosen number: " + rand);
    var finder = 0;
    var found = false;

    this.rolls.forEach((roll) => {
      if(!found) {
        var nfinder = finder+roll.amount;
        this.log(`Checking if ${rand} is between ${finder} and ${nfinder}`)
        if(finder <= rand && rand < nfinder) {
          found = true;
          this.log(`Yup, so ${roll.firstName} is the winner!`);
          this.moneyManager(roll.sid, total, () => {
            out(`🔴 💸 ROLL ! 💸 🔴\n${roll.firstName} a gagné le pari (💲${total}) avec ${this.winChance(roll.amount)}% de chance de gagner !`)
            this.rolls = [];
          })
        }
        finder = nfinder;
      }
    })
  }

  winChance(amount) {
    var total = this.getTotal();
    if(total) return Math.round(amount/total*10000)/100;
    else return 100
  }

  getTotal() {
    var total = 0;
    this.rolls.forEach((roll) => {
      total = total + roll.amount;
    })
    return total
  }

  countDown(out) {
    if(!this.config.noFlood) setTimeout(() => { out(`⏲️ Roll dans 30 secondes !`) }, 30000);
    if(this.config.noFlood) setTimeout(() => { out(`⏲️ Roll dans 20 secondes !`) }, 40000);
    else setTimeout(() => { out(`⏲️ Roll dans 15 secondes !`) }, 45000);

    if(!this.config.noFlood) {
      setTimeout(() => { out(`⏲️ Roll dans 3..`) }, 57000);
      setTimeout(() => { out(`⏲️ Roll dans 2..`) }, 58000);
      setTimeout(() => { out(`⏲️ Roll dans 1..`) }, 59000);
    } else setTimeout(() => { out(`⏲️ Roll dans 5 secondes`) }, 55000);
  }

  getUser(sid, cb) {
    this.db.collection("fb-users").find({ sid: sid, tid: this.config.thread }).toArray((err, results) => {
      this.log("getUser found " + results.length + " user(s)");
      if(results.length == 1) cb(results[0]);
      else {
        this.api.getUserInfo(sid, (err, user) => {
          user = user[sid];
          //this.log(JSON.stringify(user));
          var doc = {
            sid: sid,
            tid: this.config.thread,
            name: user.name,
            firstName: user.firstName,
            money: this.config.baseMoney,
            created: new Date()
          }
          this.db.collection("fb-users").save(doc); // create user
          this.log(`user sid #${sid} for thread #${this.config.thread} was created`)
          cb(doc);
        });
      }
    });
  }

  formatBank(data) {
    var final = "💸 BANQUE 💸\n";
    var i = 1;
    data.forEach((item) => {
      if(typeof item.money == "number" && typeof item.firstName === "string") {
        final += `${i}. ${item.firstName} (💲${item.money})\n`;
        i++;
      }
    })
    return final;
  }

  log(msg) {
    if(this.config.log) console.log(`[fb-rollbot] > ${msg}`);
  }
}
