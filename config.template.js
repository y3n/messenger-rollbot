// optionally base64 encode your password using
//const atob = require("atob")

module.exports = {
  thread: "{THREADID}",
  db: {
    host: "{DBHOST}",
    database: "{DBNAME}"
  },
  log: true,
  email: "{FBLOGIN}",
  password: "{FBPASSWORD}",
  version: "0.0.0",
  baseMoney: 2000, // initial balance
  messageMoney: 20, // money earned by message
  minAmount: 25, // minimum roll amount
  noFlood: true
}
